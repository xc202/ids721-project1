# Starting the Website

To launch the website, navigate to the "ids721-project1" directory using the command `cd ids721-project1`, and then run `zola serve`. The website will be accessible at http://127.0.0.1:1111/.

Optionally, you can access the website via the following link: [https://ids702-project1-xc202-f9be2d181fbbc023b46ad759f117425527cb20fa6.pages.oit.duke.edu](https://ids702-project1-xc202-f9be2d181fbbc023b46ad759f117425527cb20fa6.pages.oit.duke.edu)

The website is divided into three main sections: the personal homepage, the About Me page, and the My Projects page. You can navigate to these sections by clicking on the corresponding buttons.

**Note:** While SCSS files are used in the development of the website, they are compiled into CSS files for rendering the site's interface. The CSS file used for styling is "style.css" located in the "ids721-project1/usage" directory.

Below are screenshots of the entire website interface:

### Homepage:
![Screenshot 2024-01-27 at 5.04.26 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.04.26%20PM.png)

### Resume:
![Screenshot 2024-01-27 at 5.06.50 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.06.50%20PM.png)

### Projects:
![Screenshot 2024-01-27 at 5.07.32 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.07.32%20PM.png)